function findIndex(id) {
  for (var i = 0; i < listToDo.length; i++) {
    // listToDo[i].id đang ,là number vì khi lấy id từ example lấy biến index chạy
    if (+listToDo[i].id === +id) {
      return i;
    }
  }
  return -1;
}

function createCheckClass(id) {
  const index = findIndex(id);
  if (listToDo[index].toDoStatus) {
    // classList.add("checked");
    return "checked";
  }
  return "";
}

function createList(listToDo) {
  var i;
  var listContent = "";
  for (i = 0; i < listToDo.length; i++) {
    listContent += ` 
      <li class="${createCheckClass(listToDo[i].id)}" onclick = "changeStatus('${listToDo[i].id}', this)">        
        ${listToDo[i].toDoContent}
        <span class="close" 
            onclick = "removeToDoItem('${listToDo[i].id}')">\u00D7
        </span>
      </li>
    `;
  }
  document.getElementById("myUL").innerHTML = listContent;
}

//*********************************************** */

createList(listToDo);
document.getElementById("myUL").style.display = "block";

