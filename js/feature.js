// Create a new list item when clicking on the "Add" button
function newElement() {
    var inputToDo = document.getElementById("myInput").value;
    if (!inputToDo) {
      alert("You must write something!");
    } else {
      const id = new Date().getTime();
      // const newToDoItem = new ItemToDo(id, false, inputToDo);
      const newToDoItem = new ItemToDo(id, false, inputToDo);
      listToDo.push(newToDoItem);
      sessionStorage.setItem("listTD", JSON.stringify(listToDo));
      createList(listToDo);
    }
    document.getElementById("myInput").value = "";
  }
  
  function removeToDoItem(id) {
    const index = findIndex(id);
    if (index >= 0) {
      listToDo.splice(index, 1);
      createList(listToDo);
    }
    sessionStorage.setItem("listTD", JSON.stringify(listToDo));
  }
  
  //====================
  /* 
  function createChecked(){
    var list = document.getElementById("myUL");
    var listItem = list.getElementsByTagName("Li");
    // var list = document.querySelectorAll("h2");
    // var list = document.querySelector("ul");
    listItem.addEventListener(
      "click",
      function(ev) {
        if (ev.target.tagName === "LI") {
          ev.target.classList.toggle("checked");
        }
      },
      false
    );
  }
  */
  
  function changeStatus(id, obj){
    const index = findIndex(id);
    // console.log(index, listToDo[index].toDoStatus);
    listToDo[index].toDoStatus = !listToDo[index].toDoStatus;
    obj.classList.toggle("checked");
    sessionStorage.setItem("listTD", JSON.stringify(listToDo));
  }