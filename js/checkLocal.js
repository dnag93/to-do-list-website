function createExampleList() {
  var i, status, content;
  var exampleList = document.getElementsByTagName("LI");
  for (i = 0; i < exampleList.length; i++) {
    if (exampleList[i].classList.contains("checked")) {
      status = true;
    } else {
      status = false;
    }
    content = exampleList[i].innerHTML;
    const itemExample = new ItemToDo(i, status, content);
    listToDo.push(itemExample);
  }
  sessionStorage.setItem("listTD", JSON.stringify(listToDo));
}

//======================================================================

function getDataFromSession() {
  const listToDoStr = sessionStorage.getItem("listTD");
  if (listToDoStr) {
    listToDo = JSON.parse(listToDoStr);
  } else {
    createExampleList();
  }
}

//======================================================================

var listToDo = [];
getDataFromSession();
